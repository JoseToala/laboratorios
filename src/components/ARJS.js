import React, { useState } from 'react';
import { AFrameRenderer, Marker } from 'react-web-ar';
import firebase from '../components/config/firebase';

export default function ArJS (){  

  const [horarios, guardarHorarios] = useState([]);
  const consulta = ()=>{
    firebase.firestore().collection('horario').where('laboratorio','==','Laboratorio 1')
      .onSnapshot((snapshot) => {
        const datos = snapshot.docs.map((dato) => ({
          id: dato.id,
          ...dato.data()
        }))
        guardarHorarios(datos);
        console.log(horarios)
      });

      firebase.firestore().collection('horario').where('laboratorio','==','Laboratorio 2')
      .onSnapshot((snapshot) => {
        const datos = snapshot.docs.map((dato) => ({
          id: dato.id,
          ...dato.data()
        }))
        guardarHorarios(datos);
        console.log(horarios)
      });

      firebase.firestore().collection('horario').where('laboratorio','==','Laboratorio 3')
      .onSnapshot((snapshot) => {
        const datos = snapshot.docs.map((dato) => ({
          id: dato.id,
          ...dato.data()
        }))
        guardarHorarios(datos);
        console.log(horarios)
      });
  }

  return (
    <AFrameRenderer arToolKit={{ sourceType: 'webcam' }}>
      {consulta()}
      <Marker parameters={{
          preset: "pattern",
          type: "pattern",
          url: "https://bitbucket.org/JoseToala/laboratorios/raw/fb1664a25d7ed9f6a0922302ca1c25ef9808a7e4/src/components/Patts/LAB1.patt"
        }}>
          <a-text
						rotation="-100 0 0"
						color="#000EFF"
						height="2.5"
						width="2.5"
						position="-0.5 0.1 0"
					/>
      </Marker>

      <Marker parameters={{
          preset: "pattern",
          type: "pattern",
          url: "https://bitbucket.org/JoseToala/laboratorios/raw/fb1664a25d7ed9f6a0922302ca1c25ef9808a7e4/src/components/Patts/LAB2.patt"
        }}>
          <a-text
						rotation="-100 0 0"
						color="#000EFF"
						height="2.5"
						width="2.5"
						position="-0.5 0.1 0"
					/>
      </Marker>
      
      <Marker parameters={{
          preset: "pattern",
          type: "pattern",
          url: "https://bitbucket.org/JoseToala/laboratorios/raw/fb1664a25d7ed9f6a0922302ca1c25ef9808a7e4/src/components/Patts/LAB3.patt"
        }}>
          <a-text
						rotation="-100 0 0"
						color="#000EFF"
						height="2.5"
						width="2.5"
						position="-0.5 0.1 0"
					/>
      </Marker>
      
    </AFrameRenderer>
  )
}