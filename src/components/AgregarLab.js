import React,{useState} from 'react';
import Error from './Error';
import Swal from 'sweetalert2';
import {withRouter} from 'react-router-dom';
import firebase from './config/firebase';



function AgregarLab ({history, guardarRecargarProductos}){

    const [lab, guardarNombre] = useState('');
    const [detalle, guardarDetalle] = useState('');
    const [patt, guardarPatt] = useState('');
    const [error, guardarError] = useState(false);

    const agregarLaboratorio = async e=>{
        e.preventDefault();

        if(lab === ''|| detalle === '' || patt ===''){
            guardarError(true);
            return;
        }

        guardarError(false);
        
        try{
            firebase.firestore().collection('laboratorio').add({
                detalle,
                lab,
                patt
            }).then(Swal.fire(
                'Laboratorio Creado',
                'El laboratorio se creo correctamente',
                'success'
              )
            )    
            
        }catch(error){
            console.log(error);
            Swal.fire({
                type: 'error',
                title: 'Oops.',
                text: 'Hubo un error, Porfavor intente denuevo',
              }) 
        }

        //Redirigir al usuario a productos
        guardarRecargarProductos(true);
        history.push('/productos');
       
    }

    return(
        <div className="col-md-8 mx-auto ">
            <h1 className="text-center">Añadir Laboratorio</h1>

            {(error) ? <Error mensaje='Todos los campos son obligatorios'/>: null}

            <form
                className="mt-5"
                onSubmit={agregarLaboratorio}
            >
                <div className="form-group">
                    <label>Nombre del Laboratorio</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        name="nombre" 
                        placeholder="Nombre Lab"
                        onChange= {e =>guardarNombre(e.target.value)}
                    />
                </div>

                <div className="form-group">
                    <label>Detalle del Laboratorio</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        name="Detalle"
                        placeholder="Descripcion del Laboratorio"
                        onChange= {e =>guardarDetalle(e.target.value)}
                    />
                </div>

                <div className="form-group">
                    <label>Patt del laboratorio</label>
                    <textarea 
                        className="form-control" 
                        name="Patt del Laboratorio"
                        placeholder="patt"
                        onChange= {e =>guardarPatt(e.target.value)}
                    />
                </div>

                <input type="submit" className="font-weight-bold text-uppercase mt-5 btn btn-primary btn-block py-3" value="Añadir Laboratorio" />
            </form>
        </div>
        
    )

}
export default withRouter(AgregarLab);